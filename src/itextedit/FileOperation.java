package itextedit;

import jdk.nashorn.internal.ir.ContinueNode;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.io.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

/**
 * Created by Md. Asadul Islam on 4/2/2017.
 *
 * This class is used to perfom
 * some actions on files
 */
public class FileOperation {
    ITextEdit myTextEditor;
    
    boolean fileSaveFlag;
    boolean newFileFlag;
    String fileExtension;
    String fileName;
    String applicationTitle = "iTextEdit";
    
    File fileReference;
    JFileChooser fileChooserMain;
    
    /**
     * Constructor function
     * @param myTextEditor reference to main class
     */
    FileOperation (ITextEdit myTextEditor) {
        this.myTextEditor = myTextEditor;
        
        //updating text area every time after any change
        this.myTextEditor.textAreaMain.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate (DocumentEvent e) {
                fileSaveFlag = false;
                myTextEditor.setTitle("* "+fileName+ " - "+applicationTitle);
            }
        
            @Override
            public void removeUpdate (DocumentEvent e) {
                fileSaveFlag = false;
                myTextEditor.setTitle("* "+fileName+" - "+applicationTitle);
            }
        
            @Override
            public void changedUpdate (DocumentEvent e) {
                fileSaveFlag = false;
                myTextEditor.setTitle("* "+fileName+" - "+applicationTitle);
            }
        });
        
        fileSaveFlag = true;
        newFileFlag = true;
        fileName = new String("New File");
        fileReference = new File(fileName);
        myTextEditor.setTitle(fileName + " - " + applicationTitle);
        
        //adding file filter to file chooser
        fileChooserMain = new JFileChooser();
        MyFileFilter filter = new MyFileFilter(".txt", "Text Files(*.txt)");
        fileChooserMain.addChoosableFileFilter(filter);
        fileChooserMain.setFileFilter(filter);
        filter = new MyFileFilter(".java", "Java Source File(*.java)");
        fileChooserMain.addChoosableFileFilter(filter);
        filter = new MyFileFilter(".c", "C Code(*.c)");
        fileChooserMain.addChoosableFileFilter(filter);
        fileChooserMain.setCurrentDirectory(new File("."));
    }
    
    /**
     * This method creates new file
     */
    void createNewFile () {
        if ( confirmSave()==2 ) return;
        
        myTextEditor.textAreaMain.setText("");
        fileName = new String("New File");
        fileReference = new File(fileName);
        fileSaveFlag = false;
        newFileFlag = true;
        myTextEditor.setTitle(fileName + " - " + applicationTitle);
    }
    
    /**
     * This method open selected file
     *
     * @param temp Selected File
     * @return true if file opened or false if not
     * @exception IOException on input/output error
     * @see IOException
     */
    boolean openFile (File temp) {
        FileInputStream fin = null;
        BufferedReader din = null;
        try {
            fin = new FileInputStream(temp);
            din = new BufferedReader(new InputStreamReader(fin));
            String text = "";
            do {
                text = din.readLine();
                if ( text == null )
                    break;
                ITextEdit.textAreaMain.append(text + "\n");
            } while ( text != null );
            
        } catch ( IOException ioe ) {
            updateStatus(temp, false);
            return false;
        } finally {
            try {
                din.close();
                fin.close();
            } catch ( IOException excp ) {
                
            }
        }
        updateStatus(temp, true);
        ITextEdit.textAreaMain.setCaretPosition(0);
        newFileFlag = false;
        return true;
    }
    
    /**
     * This method checks if the existing file is saved or not
     * before opening another file
     *
     * If not saved prompt to save the file
     */
    void openFile () {
        if ( confirmSave()==2 )
            return;
        fileChooserMain.setDialogTitle("Open File...");
        fileChooserMain.setApproveButtonText("Open this");
        fileChooserMain.setApproveButtonMnemonic(KeyEvent.VK_O);
        fileChooserMain.setApproveButtonToolTipText("Click to open the selected file");
        
        File temp = null;
        do {
            if ( fileChooserMain.showOpenDialog(myTextEditor) != JFileChooser.APPROVE_OPTION )
                return;
            temp = fileChooserMain.getSelectedFile();
            
            if ( temp.exists() )
                break;
            JOptionPane.showMessageDialog(myTextEditor,
                    "<html>" + temp.getName() + "<br>file not found.<br>" +
                            "<html>",
                    "Open", JOptionPane.INFORMATION_MESSAGE);
            
        } while ( true );
        
        myTextEditor.textAreaMain.setText("");
        
        if ( !openFile(temp) ) {
            fileName = "New File";
            fileSaveFlag = true;
            this.myTextEditor.setTitle(fileName + " - " + applicationTitle);
        }
        if ( !temp.canWrite() )
            newFileFlag = true;
    }
    
    /**
     * This method save a file
     * and return boolean value after action
     * @return
     */
    boolean saveThisFile () {
        
        if ( !newFileFlag ) {
            return saveFile(fileReference);
        }
        return saveAsFile();
    }
    
    /**
     * This method returns true or false
     * according to saving the existing file
     * @param temp This file should be saved
     * @return boolean value
     * @exception IOException on input error
     * @see IOException
     */
    
    boolean saveFile (File temp) {
        FileWriter fout = null;
        try {
            fout = new FileWriter(temp);
            fout.write(myTextEditor.textAreaMain.getText());
        } catch ( IOException ioe ) {
            updateStatus(temp, false);
            return false;
        } finally {
            try {
                fout.close();
            } catch ( IOException excp ) {
            }
        }
        updateStatus(temp, true);
        return true;
    }
    
    /**
     * This method call saveAsFile() if new file
     * else call saveFile()
     * @return return boolean value if file successfully saved or not
     */
    boolean saveAsFile () {
        File temp = null;
        fileChooserMain.setDialogTitle("Save As...");
        fileChooserMain.setApproveButtonText("Save Now");
        fileChooserMain.setApproveButtonMnemonic(KeyEvent.VK_S);
        fileChooserMain.setApproveButtonToolTipText("Click to save the file!");
        
        do {
            if ( fileChooserMain.showSaveDialog(myTextEditor) != JFileChooser.APPROVE_OPTION )
                return false;
            if (!newFileFlag)
                temp = fileChooserMain.getSelectedFile();
            else
                fileName = "";
            if ( !temp.exists() )
                break;
            if ( JOptionPane.showConfirmDialog(
                    myTextEditor, "<html>" + temp.getPath() + " already exists.<br>Do you want to replace it?<html>",
                    "Save As", JOptionPane.YES_NO_OPTION
            ) == JOptionPane.YES_OPTION )
                break;
        } while ( true );
        return saveFile(temp);
    }
    
    /**
     * @return return integer value according to saving option
     */
    int confirmSave () {
        String strMsg = "<html>Do you want to save the changes?<html>";
        if ( !fileSaveFlag ) {
            int x = JOptionPane.showConfirmDialog(myTextEditor, strMsg, applicationTitle, JOptionPane.YES_NO_CANCEL_OPTION);
            
            if ( x == JOptionPane.NO_OPTION )
                return 0;
            else if ( x == JOptionPane.YES_OPTION && saveThisFile() )
                return 1;
            else if(x==JOptionPane.CANCEL_OPTION) {
                return 2;
            }
            else
                return 3;
        }
        return 1;
    }
    
    
    /**
     * update file status
     * after saving a file
     * or editing some text
     */
    void updateStatus (File temp, boolean fileSaveFlag) {
        if ( fileSaveFlag ) {
            this.fileSaveFlag = true;
            fileName = new String(temp.getName());
            if ( !temp.canWrite() ) {
                fileName += "(Read only)";
                //newFileFlag = true;
            }
            fileReference = temp;
            myTextEditor.setTitle(fileName + " - " + applicationTitle);
            //JOptionPane.showMessageDialog(null,"\""+fileName+"\"  is saved successfully.");
            newFileFlag = false;
        } else {
            JOptionPane.showMessageDialog(null,"Failed to save" + temp.getPath());
        }
        
        
    }
    
    
    /**
     *
     * @return
     */

    boolean isSave () {
        return fileSaveFlag;
    }
    
    void setSave (boolean fileSaveFlag) {
        this.fileSaveFlag = fileSaveFlag;
    }
    
    String getFileName () {
        return new String(fileName);
    }
    
    void setFileName (String fileName) {
        this.fileName = new String(fileName);
    }
    
    String getFileExtension(){
        return new MyFileFilter().getExtension();
    }
    
}


//****************************   My FileFilter Class  ******************************//


/**
 * This class demonstrates file filtering
 * @author Md. Asadul Islam
 *
 */
class MyFileFilter extends FileFilter{
    private String extension;
    private String description;
    
    //constructor
    public MyFileFilter () {
        setExtension(null);
        setDescription(null);
    }
    
    public MyFileFilter (final String ext, final String desc) {
        setExtension(ext);
        setDescription(desc);
    }
    
    /**
     * returns true if the file should be displayed
     * or false
     * @param f The file which to be filterd
     * @return
     */
    public boolean accept (File f) {
        final String filename = f.getName();
    
        return f.isDirectory() ||
                extension == null ||
                filename.toUpperCase()
                        .endsWith(extension.toUpperCase());
    
    }
    
    ////////////////
    public String getDescription () {
        return description;
    }
    
    ////////////////
    public void setDescription (String desc) {
        if ( desc == null )
            description = new String("All Files(*.*)");
        else
            description = new String(desc);
    }
    
    ///////////////r
    public void setExtension (String ext) {
        if ( ext == null ) {
            extension = ".txt";
            return;
        }
        
        extension = new String(ext).toLowerCase();
        /*if ( !ext.startsWith(".") )
            extension = "." + extension;*/
    }
    
    public String getExtension(){
        return extension;
    }
////////////////
}
