package itextedit;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.event.*;


/**
 * Created by Md. Asadul Islam on 4/1/2017.
 *
 * This is the main class of this project
 * This class demonstrates GUI components
 *
 * It also includes some simple actions
 */
public class ITextEdit extends JFrame {
    
    /**
     * Important Variables
     */
    static JTextArea textAreaMain;
    JMenuBar menuBarMain;
    JMenu mnuFile, mnuEdit, mnuFormat, mnuSetting, mnuHelp;
    JMenuItem itemNew, itemOpen, itemSave, itemSaveAs, itemExit,
            itemUndo, itemRedo, itemCut, itemCopy, itemPaste, itemDelete, ItemSelectAll, itemGoto, itemFind, itemReplace,
            itemFontColor, itemFontChooser;
    JCheckBoxMenuItem cbItemWordWrap;
    JLabel statusBar;
    JToolBar toolBarMain;
    JButton btnNew, btnOpen, btnSave,
            btnUndo, btnRedo, btnCopy, btnCut, btnPaste, btnFind, btnReplace;
    String fileName, textFileContent, iconPath;
    UndoManager undoRedoManager;
    int columnNumber, rowNumber, linePosition;
    FontChooser fontChooserMain;
    FileOperation fileHandler;
    FindAndReplace findAndReplace;
    
    /**
     * ActionListener for toolbar button
     * and menu item
     */
    ActionListener btnItemAction = new ActionListener() {
        /**
         * Performs action on event
         * @param e Event
         */
        @Override
        public void actionPerformed (ActionEvent e) {
            if ( e.getSource() == btnNew || e.getSource() == itemNew ) {
                fileHandler.createNewFile();
            } else if ( e.getSource() == btnOpen || e.getSource() == itemOpen ) {
                fileHandler.openFile();
            } else if ( e.getSource() == btnSave || e.getSource() == itemSave ) {
                fileHandler.saveThisFile();
            } else if ( e.getSource() == itemSaveAs ) {
                fileHandler.saveAsFile();
            } else if ( e.getSource() == itemExit ) {
                System.exit(0);
            } else if ( e.getSource() == btnUndo || e.getSource() == itemUndo ) {
                try {
                    undoRedoManager.undo();
                } catch ( CannotRedoException cre ) {
                    cre.printStackTrace();
                }
                undoRedoUpdate();
            } else if ( e.getSource() == btnRedo || e.getSource() == itemRedo ) {
                try {
                    undoRedoManager.redo();
                } catch ( CannotRedoException cre ) {
                    cre.printStackTrace();
                }
                undoRedoUpdate();
            } else if ( e.getSource() == btnCut || e.getSource() == itemCut ) {
                textAreaMain.cut();
            } else if ( e.getSource() == btnCopy || e.getSource() == itemCopy ) {
                textAreaMain.copy();
            } else if ( e.getSource() == btnPaste || e.getSource() == itemPaste ) {
                try {
                    textAreaMain.paste();
                } catch ( Exception ex ) {
                
                }
            } else if ( e.getSource() == btnFind || e.getSource() == itemFind ) {
                findReplace(btnFind);
            } else if ( e.getSource() == btnReplace || e.getSource() == itemReplace ) {
                findReplace(btnReplace);
            } else if ( e.getSource() == ItemSelectAll ) {
                textAreaMain.selectAll();
            } else if ( e.getSource() == itemGoto ) {
                goTo();
            } else {
            
            }
        }
    };
    
    /**
     * MouseListener for toolbar button
     */
    MouseListener btnMouseAction = new MouseAdapter() {
        @Override
        public void mouseEntered (MouseEvent e) {
            if ( e.getSource() == btnNew ) {
                btnNew.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnNew.setBackground(Color.gray);
            } else if ( e.getSource() == btnOpen ) {
                btnOpen.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                //btnOpen.setBorder(BorderFactory.createSoftBevelBorder(0));
                btnOpen.setBackground(Color.gray);
            } else if ( e.getSource() == btnSave ) {
                btnSave.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnSave.setBackground(Color.gray);
            } else if ( e.getSource() == btnUndo ) {
                btnUndo.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnUndo.setBackground(Color.gray);
            } else if ( e.getSource() == btnRedo ) {
                btnRedo.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnRedo.setBackground(Color.gray);
            } else if ( e.getSource() == btnCut ) {
                btnCut.setBackground(Color.gray);
                btnCut.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            } else if ( e.getSource() == btnCopy ) {
                btnCopy.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnCopy.setBackground(Color.gray);
            } else if ( e.getSource() == btnPaste ) {
                btnPaste.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnPaste.setBackground(Color.gray);
            } else if ( e.getSource() == btnReplace ) {
                btnReplace.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnReplace.setBackground(Color.gray);
            } else if ( e.getSource() == btnFind ) {
                btnFind.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                btnFind.setBackground(Color.gray);
            } else {}
        }
        
        @Override
        public void mouseExited (MouseEvent e) {
            if ( e.getSource() == btnNew ) {
                btnNew.setBackground(null);
                btnNew.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
            } else if ( e.getSource() == btnOpen ) {
                btnOpen.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnOpen.setBackground(null);
            } else if ( e.getSource() == btnSave ) {
                btnSave.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnSave.setBackground(null);
            } else if ( e.getSource() == btnUndo ) {
                btnUndo.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnUndo.setBackground(null);
            } else if ( e.getSource() == btnRedo ) {
                btnRedo.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnRedo.setBackground(null);
            } else if ( e.getSource() == btnCut ) {
                btnCut.setBackground(null);
                btnCut.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
            } else if ( e.getSource() == btnCopy ) {
                btnCopy.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnCopy.setBackground(null);
            } else if ( e.getSource() == btnPaste ) {
                btnPaste.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnPaste.setBackground(null);
            } else if ( e.getSource() == btnReplace ) {
                btnReplace.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnReplace.setBackground(null);
            } else if ( e.getSource() == btnFind ) {
                btnFind.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
                btnFind.setBackground(null);
            } else{}
        }
    };
    
    
    /**
     * Constructor
     */
    public ITextEdit () {
        initComponent();
        
        /*listiner for text area
        manages undo redo actions*/
        textAreaMain.getDocument().addUndoableEditListener(new UndoableEditListener() {
            @Override
            public void undoableEditHappened (UndoableEditEvent e) {
                undoRedoManager.addEdit(e.getEdit());
                undoRedoUpdate();
            }
            
        });
        
        
        /// track current cursor position
        textAreaMain.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate (CaretEvent e) {
                int pos = 0;
                if ( textAreaMain.getText().length() == 0 ) {
                    rowNumber = 0;
                    columnNumber = 0;
                    updateEnabled(false);
                } else {
                    try {
                        updateEnabled(true);
                        pos = textAreaMain.getCaretPosition();
                        rowNumber = textAreaMain.getLineOfOffset(pos);
                        columnNumber = pos - textAreaMain.getLineStartOffset(rowNumber);
                    } catch ( Exception ex ) {
                        ex.printStackTrace();
                    }
                }
                statusBar.setText("Ln : " + ( rowNumber + 1 ) + "          ||        col : " + ( columnNumber + 1 ));
            }
        });
        
        //actionlitener for file menu item
        itemNew.addActionListener(btnItemAction);
        itemOpen.addActionListener(btnItemAction);
        itemSave.addActionListener(btnItemAction);
        itemSaveAs.addActionListener(btnItemAction);
        itemExit.addActionListener(btnItemAction);
        
        //add action listener for edit menu item
        itemUndo.addActionListener(btnItemAction);
        itemRedo.addActionListener(btnItemAction);
        itemCopy.addActionListener(btnItemAction);
        itemCut.addActionListener(btnItemAction);
        itemPaste.addActionListener(btnItemAction);
        itemDelete.addActionListener(btnItemAction);
        ItemSelectAll.addActionListener(btnItemAction);
        itemGoto.addActionListener(btnItemAction);
        itemFind.addActionListener(btnItemAction);
        itemReplace.addActionListener(btnItemAction);
        
        // action listener for format menu
        cbItemWordWrap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                if ( cbItemWordWrap.isSelected() ) {
                    textAreaMain.setWrapStyleWord(true);
                    textAreaMain.setLineWrap(true);
                } else {
                    textAreaMain.setWrapStyleWord(false);
                    textAreaMain.setLineWrap(false);
                }
            }
        });
        
        //action listener for font color chooser
        itemFontColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                Color fontColor = JColorChooser.showDialog(rootPane, "Choose Font Color", Color.BLACK);
                textAreaMain.setForeground(fontColor);
            }
        });
        
        ///action listener for font chooser menu item
        itemFontChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                fontChooserMain.setLocation(getX() + 100, getY() + 100);
                fontChooserMain.setVisible(true);
            }
        });
        
        
        //choosing font, style and size
        fontChooserMain.getBtnOK().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                textAreaMain.setFont(fontChooserMain.getChooseFont());
                fontChooserMain.setVisible(false);
            }
        });
        fontChooserMain.getBtnCancel().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                fontChooserMain.setVisible(false);
            }
        });
    }
    
    /**
     * Main Function
     * @param args not used
     */
    public static void main (String[] args) {
        new ITextEdit();
    }
    
    /**
     * initialize main components
     * <p>
     * set poperty to the components
     */
    private void initComponent () {
        
        /**  important variables
         *   value assign
         */
        
        iconPath = "itextedit/Resources/";
        fileName = "New File";
        textFileContent = "";
        columnNumber = 0;
        rowNumber = 0;
        linePosition = 0;
        statusBar = new JLabel("Ln : " + ( rowNumber + 1 ) + "          ||        col : " + ( columnNumber + 1 ), JLabel.CENTER);
        statusBar.setFont(new Font("Arial", Font.PLAIN, 14));
        statusBar.setBorder(BorderFactory.createEtchedBorder());
        undoRedoManager = new UndoManager();
        
        //****  main text area  ****//
        textAreaMain = new JTextArea();
        textAreaMain.setBorder(null);
        getContentPane().add(textAreaMain);
        getContentPane().add(new JScrollPane(textAreaMain), BorderLayout.CENTER);
        setTitle(fileName + " - iTextEdit");
        fileHandler = new FileOperation(this);
        
        //////////*****   menubar and menus  *****/////////
        menuBarMain = new JMenuBar();
        mnuFile = new JMenu(" File");
        mnuFile.setMnemonic(KeyEvent.VK_F);
        mnuEdit = new JMenu("Edit");
        mnuEdit.setMnemonic(KeyEvent.VK_E);
        mnuFormat = new JMenu("Format");
        //mnuFormat.setMnemonic(KeyEvent.VK_F);
        mnuSetting = new JMenu("Setting");
        mnuHelp = new JMenu("Help");
        
        ///****** file menu item  ******///
        itemNew = new JMenuItem("    New");
        itemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        itemNew.setToolTipText("Open New File");
        itemOpen = new JMenuItem("    Open");
        itemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        itemOpen.setToolTipText("Open Existing File");
        itemSave = new JMenuItem("    Save");
        itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        itemSave.setToolTipText("Save File");
        itemSaveAs = new JMenuItem("    Save As");
        itemSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
        itemSaveAs.setToolTipText("Save File As");
        itemExit = new JMenuItem("    Exit");
        itemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        itemExit.setToolTipText("Exit from Aplication");
        
        //add to file menu
        mnuFile.add(itemNew);
        mnuFile.add(itemOpen);
        mnuFile.add(itemSave);
        mnuFile.add(itemSaveAs);
        mnuFile.addSeparator();
        mnuFile.add(itemExit);
        
        ///***** edit menu item   ******///
        itemUndo = new JMenuItem("    Undo");
        itemUndo.setEnabled(false);
        itemRedo = new JMenuItem("    Redo");
        itemRedo.setEnabled(false);
        itemCopy = new JMenuItem("    Copy");
        itemCopy.setEnabled(false);
        itemCut = new JMenuItem("    Cut");
        itemCut.setEnabled(false);
        itemPaste = new JMenuItem("    Paste");
        itemDelete = new JMenuItem("    Delete");
        itemDelete.setEnabled(false);
        ItemSelectAll = new JMenuItem("    Select All");
        ItemSelectAll.setEnabled(false);
        itemGoto = new JMenuItem("    Go to...");
        itemGoto.setEnabled(false);
        itemFind = new JMenuItem("    Find");
        itemFind.setEnabled(false);
        itemReplace = new JMenuItem("    Replace");
        itemReplace.setEnabled(false);
        
        //add shortcut
        itemUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
        itemRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
        itemCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        itemCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        itemPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
        itemDelete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        ItemSelectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        itemFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
        itemReplace.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        
        //add menu item to edit menu
        mnuEdit.add(itemUndo);
        mnuEdit.add(itemRedo);
        mnuEdit.addSeparator();
        mnuEdit.add(itemCopy);
        mnuEdit.add(itemCut);
        mnuEdit.add(itemPaste);
        mnuEdit.add(itemDelete);
        mnuEdit.add(ItemSelectAll);
        mnuEdit.addSeparator();
        mnuEdit.add(itemGoto);
        mnuEdit.add(itemFind);
        mnuEdit.add(itemReplace);
        
        ///******   format menu item    ******///
        cbItemWordWrap = new JCheckBoxMenuItem("    Word Wrap");
        itemFontColor = new JMenuItem("    Font Color");
        itemFontChooser = new JMenuItem("    Font");
        fontChooserMain = new FontChooser();
        
        //add to format menu
        mnuFormat.add(cbItemWordWrap);
        mnuFormat.add(itemFontColor);
        mnuFormat.add(itemFontChooser);
        
        //////// add menus to menubar   ///////
        menuBarMain.add(mnuFile);
        menuBarMain.add(mnuEdit);
        menuBarMain.add(mnuFormat);
        menuBarMain.add(mnuSetting);
        menuBarMain.add(mnuHelp);
        
        // add menubar to jframe
        setJMenuBar(menuBarMain);
        
        //////////  toolbar  //////////
        toolBarMain = new JToolBar();
        toolBarMain.setBorderPainted(false);
        toolBarMain.setBorderPainted(true);
        toolBarMain.setBorder(BorderFactory.createLineBorder(Color.gray));
        toolBarMain.setBackground(Color.lightGray);
        /**
         * the next line will show toolbar
         * for showing toolbar, just comment it out
         */
        //getContentPane().add(toolBarMain, BorderLayout.NORTH);
        
        ///***** main toolbar item  *****///
        JLabel seperator1 = new JLabel("|");
        seperator1.setFont(new Font("Arial", Font.PLAIN, 20));
        seperator1.setForeground(Color.GRAY);
        toolBarMain.addSeparator(new Dimension(8, 20));
        //createToolberButtonProperty("New.png",btnNew,"Click to create new file",true);
        btnNew = new JButton();
        setButtonProperty(btnNew, "New.png",  "New File", true);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnOpen = new JButton();
        setButtonProperty(btnOpen, "Open.png",  "Open File", true);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnSave = new JButton();
        setButtonProperty(btnSave, "Save.png",  "Save", true);
        toolBarMain.addSeparator(new Dimension(10, 20));
        toolBarMain.add(seperator1);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        JLabel seperator2 = new JLabel("|");
        seperator2.setFont(new Font("Arial", Font.PLAIN, 20));
        seperator2.setForeground(Color.GRAY);
        btnUndo = new JButton();
        setButtonProperty(btnUndo, "Undo.png",  "Undo", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnRedo = new JButton();
        setButtonProperty(btnRedo, "Redo.png",  "Redo", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        toolBarMain.add(seperator2);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        JLabel seperator3 = new JLabel("|");
        seperator3.setFont(new Font("Arial", Font.PLAIN, 20));
        seperator3.setForeground(Color.GRAY);
        btnCopy = new JButton();
        setButtonProperty(btnCopy, "Copy.png",  "Copy", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnCut = new JButton();
        setButtonProperty(btnCut, "Cut.png",  "Cut", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnPaste = new JButton();
        setButtonProperty(btnPaste, "Paste.png",  "Paste", true);
        toolBarMain.addSeparator(new Dimension(10, 20));
        toolBarMain.add(seperator3);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        JLabel seperator4 = new JLabel("|");
        seperator4.setFont(new Font("Arial", Font.PLAIN, 20));
        seperator4.setForeground(Color.GRAY);
        btnFind = new JButton();
        setButtonProperty(btnFind, "Find.png",  "Find", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        btnReplace = new JButton();
        setButtonProperty(btnReplace, "Replace.png",  "Find & Replace", false);
        toolBarMain.addSeparator(new Dimension(10, 20));
        
        
        ///// adding item to JFrame   //////
        getContentPane().add(statusBar, BorderLayout.SOUTH);
        add(new JLabel("  "), BorderLayout.WEST);
        add(new JLabel("  "), BorderLayout.EAST);
        pack();
        setMinimumSize(new Dimension(500,400));
        setSize(800, 600);
        
        /* window listener for closing window
         *  checking if the existing file is saved or not
         */
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing (WindowEvent e) {
                int cb = fileHandler.confirmSave();
                if(cb==1 || cb==0)
                {
                    System.exit(0);
                    return;
                }
                else{
                    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                }
            }
        });
        
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    
    /**
     * This method set button property
     * @param button on which properties are be applied
     * @param iconName name of the button icon
     * @param toolTip Button tooltip
     * @param isEnable if the button is enable or disable at start
     */
    private void setButtonProperty(JButton button, String iconName, String toolTip, boolean isEnable){
        button.setIcon(new ImageIcon(getClass().getClassLoader().getResource(iconPath+iconName)));
        button.setEnabled(isEnable);
        button.setBorder(BorderFactory.createLineBorder(toolBarMain.getBackground()));
        button.setToolTipText(toolTip);
        button.setBackground(toolBarMain.getBackground());
        button.addActionListener(btnItemAction);
        button.addMouseListener(btnMouseAction);
        toolBarMain.add(button);
    }
    
    /**
     * change undo redo situation
     */
    public void undoRedoUpdate () {
        itemUndo.setEnabled(undoRedoManager.canUndo());
        itemRedo.setEnabled(undoRedoManager.canRedo());
        btnUndo.setEnabled(undoRedoManager.canUndo());
        btnRedo.setEnabled(undoRedoManager.canRedo());
    }
    
    /**
     * This method enable and disable buttons
     * after every action such as
     * cut, copy, paste, undo, redo etc has occured
     *
     * @param flag Condition of button(enable/disable)
     */
    void updateEnabled (boolean flag) {
        if ( flag ) {
            itemCopy.setEnabled(true);
            itemCut.setEnabled(true);
            ItemSelectAll.setEnabled(true);
            itemGoto.setEnabled(true);
            itemFind.setEnabled(true);
            itemReplace.setEnabled(true);
            btnCopy.setEnabled(true);
            btnCut.setEnabled(true);
            btnFind.setEnabled(true);
            btnReplace.setEnabled(true);
        } else {
            itemCopy.setEnabled(false);
            itemCut.setEnabled(false);
            ItemSelectAll.setEnabled(false);
            itemGoto.setEnabled(false);
            itemFind.setEnabled(false);
            itemReplace.setEnabled(false);
            btnCopy.setEnabled(false);
            btnCut.setEnabled(false);
            btnFind.setEnabled(false);
            btnReplace.setEnabled(false);
        }
    }
    
    
    /**
     * This method shows find and replace dialog box
     *
     * @param btn To specify action
     */
    void findReplace(JButton btn){
        if(btn==btnFind){
            findAndReplace = new FindAndReplace(null, false);
        }
        else if ( btn == btnReplace ){
            findAndReplace = new FindAndReplace(null, true);
        }
        int getXPosition = ( getWidth() / 2 ) - ( findAndReplace.getWidth() / 2 ) + getX();
        int getYPosition = ( getHeight() / 2 ) - ( findAndReplace.getHeight() / 2 ) + getY();
        findAndReplace.setLocation(getXPosition, getYPosition);
        findAndReplace.setResizable(false);
        findAndReplace.setVisible(true);
    }
    
    
    /**
     * This method help an user to
     * move the caret position at specific line
     * accornding to line number input by user
     */
    void goTo () {
        int lineNumber = 0;
        try {
            lineNumber = textAreaMain.getLineOfOffset(textAreaMain.getCaretPosition()) + 1;
            String tempStr = JOptionPane.showInputDialog("Enter Line Number:", "" + lineNumber);
            if ( tempStr == null ) {
                return;
            }
            lineNumber = Integer.parseInt(tempStr);
            textAreaMain.setCaretPosition(textAreaMain.getLineStartOffset(lineNumber - 1));
        } catch ( Exception e ) {
        }
    }
}
