package itextedit;

import javax.jnlp.JNLPRandomAccessFile;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Md. Asadul Islam on 4/2/2017.
 */
public class FindTest extends JDialog implements ActionListener{
    JTextField searchText, replaceText;
    JCheckBox caseSensitive, caseInSensitive;
    JRadioButton searchUp, searchDown;
    JLabel searchStatus;
    boolean isFoundOne, isReplace;
    JPanel northPanel, centerPanel, southPanel;
    
    public FindTest(JFrame owner, boolean isReplace){
        super(owner, true);
        this.isReplace = isReplace;
        northPanel = new JPanel();
        centerPanel  = new JPanel();
        southPanel = new JPanel();
        
        if(isReplace){
            setTitle("  Find and Replace");
            setReplacePanel(northPanel);
        }
        else {
            setTitle("  Find");
            findSearchText(northPanel);
        }
        
        addcomponent(centerPanel);
        
        searchStatus = new JLabel();
        southPanel.add(searchStatus);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing (WindowEvent e) {
                dispose();
            }
        });
        
        getContentPane().add(northPanel, BorderLayout.NORTH);
        getContentPane().add(centerPanel, BorderLayout.CENTER);
        getContentPane().add(southPanel, BorderLayout.SOUTH);
        pack();  //allocate actually needed space/jaiga
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    
    private void addcomponent(JPanel centerPanel){
        JPanel eastPanel = new JPanel();
        JPanel westPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(1,2));
        eastPanel.setLayout(new GridLayout(2,1));
        westPanel.setLayout(new GridLayout(2,1));
        caseSensitive = new JCheckBox("Match Case",false);
        caseInSensitive = new JCheckBox("match word",false);
        ButtonGroup group = new ButtonGroup();
        searchUp = new JRadioButton("Search Up", false);
        searchDown = new JRadioButton("Search Down",true);
        group.add(searchUp);
        group.add(searchDown);
        eastPanel.add(caseSensitive);
        eastPanel.add(caseInSensitive);
        eastPanel.setBorder(BorderFactory.createTitledBorder("Search Options :"));
        
        westPanel.add(searchUp);
        westPanel.add(searchDown);
        westPanel.setBorder(BorderFactory.createTitledBorder("Search Direction ; "));
        centerPanel.add(westPanel);
        centerPanel.add(eastPanel);
    }
    
    private void findSearchText(JPanel northPanel){
        final JButton FIND_NEXT = new  JButton("Find Next");
        FIND_NEXT.addActionListener(this);
        FIND_NEXT.setEnabled(false);
        searchText = new JTextField(12);
        searchText.addActionListener(this);
        searchText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased (KeyEvent e) {
                boolean state = (searchText.getDocument().getLength()>0);
                FIND_NEXT.setEnabled(true);
                isFoundOne = false;
            }
            @Override
            public void keyPressed(KeyEvent e){
                
            }
        });
        
        if(searchText.getText().length()>0)
        {
            FIND_NEXT.setEnabled(true);
        }
        northPanel.setLayout(new FlowLayout());
        northPanel.add(new JLabel("Search Text : "));
        northPanel.add(searchText);
        northPanel.add(FIND_NEXT);
    }
    
    private void setReplacePanel(JPanel northPanel) {
        GridBagLayout grid = new GridBagLayout();
        northPanel.setLayout(grid);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel labelFindWord = new JLabel("Search Text :");
        JLabel labelReplaceWord = new JLabel("Replace With :");
        final JButton FIND_NEXT = new JButton("Find Next");
        FIND_NEXT.addActionListener(this);
        FIND_NEXT.setEnabled(false);
        final JButton REPLACE_NEXT = new JButton("Replace Text");
        REPLACE_NEXT.addActionListener(this);
        REPLACE_NEXT.setEnabled(false);
        final JButton REPLACE_ALL = new JButton("Replace All");
        REPLACE_ALL.addActionListener(this);
        REPLACE_ALL.setEnabled(false);
        searchText = new JTextField(12);
        replaceText = new JTextField(12);
        replaceText.addKeyListener( new KeyAdapter() {
            @Override
            public void keyReleased (KeyEvent e) {
                boolean state = (replaceText.getDocument().getLength()>0);
                FIND_NEXT.setEnabled(state);
                REPLACE_NEXT.setEnabled(state);
                REPLACE_ALL.setEnabled(state);
                isFoundOne = false;
            }
        });
        
        constraints.gridx = 0;
        constraints.gridy = 0;
        grid.setConstraints(labelFindWord,constraints);
        northPanel.add(labelFindWord);
        
        constraints.gridx = 1;
        constraints.gridy = 0;
        grid.setConstraints(searchText,constraints);
        northPanel.add(searchText);
        
        constraints.gridx = 2;
        constraints.gridy = 0;
        grid.setConstraints(FIND_NEXT,constraints);
        northPanel.add(FIND_NEXT);
        
        constraints.gridx = 0;
        constraints.gridy = 1;
        grid.setConstraints(labelReplaceWord,constraints);
        northPanel.add(labelReplaceWord);
        
        constraints.gridx = 1;
        constraints.gridy = 1;
        grid.setConstraints(replaceText,constraints);
        northPanel.add(replaceText);
        
        constraints.gridx = 2;
        constraints.gridy = 1;
        grid.setConstraints(REPLACE_NEXT,constraints);
        northPanel.add(REPLACE_NEXT);
        
        constraints.gridx = 2;
        constraints.gridy = 2;
        grid.setConstraints(REPLACE_ALL,constraints);
        northPanel.add(REPLACE_ALL);
        
        
    }
    
    private void setReplaceAll(){
        String searchingWord = searchText.getText();
        String searchingText = ITextEdit.textAreaMain.getText();
        String replacementText = replaceText.getText();
        StringBuffer stringBuffer = new StringBuffer(searchingText);
        int diff = replacementText.length() - searchingWord.length();
        int offset = 0;
        int countReplacrWord = 0;
        for(int i =0; i<(searchingText.length()-searchingWord.length()); i++){
            String temp = searchingText.substring(i,i+searchingWord.length());
            if((temp.equals(searchingWord) && checkForWholeWord(searchingWord.length(), searchingText,0,i)))
            {
                countReplacrWord++;
                stringBuffer.replace(i+offset,i+offset+searchingWord.length(), replacementText);
                offset+=diff;
            }
        }
        ITextEdit.textAreaMain.setText(stringBuffer.toString());
        countReplaceAll(true, countReplacrWord);
        ITextEdit.textAreaMain.setCaretPosition(0);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(searchText) || e.getSource().equals(replaceText) ){
            validate();  //validate all components
        }
        searchProcess();
        if(e.getActionCommand().equals("Replace All")){
            setReplaceAll();
        }
    }
    
    private void searchProcess(){
        if(isReplace){
            searchStatus.setText("Replacing " + searchText.getText());
        }
        else {
            searchStatus.setText("Searching " + searchText.getText());
        }
        
        int caretPosition = ITextEdit.textAreaMain.getCaretPosition();
        String searchingWord = getSearchingWord();
        String searchingText = getSearchingText();
        caretPosition = searchTextMethod(searchingText, searchingWord, caretPosition);
        if(caretPosition<0)
        {
            countReplaceAll(false,0);
        }
    }
    
    private void countReplaceAll(boolean isReplaceAll, int countFoundAll){
        String message = "";
        if(isReplaceAll){
            if(countFoundAll==0){
                message = searchText.getText()+ " Not found.";
            }
            else if(countFoundAll == 1){
                message = "One change was happened";
            }
            else {
                message = "" + countFoundAll + " changes was happened";
            }
        }
        else {
            String string = "";
            if(isSearchDown()){
                string = "Search Down";
            }
            else {
                string = "Search Up";
            }
            if(isFoundOne && !isReplace){
                message = "End of File"; //+string+" for "+searchText.getText();
            }
            else if ( isFoundOne && isReplace ){
                message = "End of Replace "+searchText.getText()+" with "+replaceText.getText();
            }
        }
        searchStatus.setText(message);
    }
    
    private int searchTextMethod(String serarchingText, String searchingWord, int caretPosition){
        boolean isFound = false;
        int textLength = serarchingText.length();
        int wordLength = searchingWord.length();
        if(isSearchDown()){
            int add = 0;
            for(int i = caretPosition; i<textLength; i++){
                String temp = serarchingText.substring(i, (i+wordLength));
                if(temp.equals(searchingWord)){
                    if(wholeWordIsSelected()){
                        if(checkForWholeWord(wordLength, serarchingText, add, caretPosition)){
                            caretPosition = i;
                            isFound = true;
                            break;
                        }
                    }
                    else {
                        caretPosition = i;
                        isFound = true;
                        break;
                    }
                }
            }
        }
        else {
            int add = caretPosition;
            for(int i = caretPosition-1; i>=wordLength; i--){
                add--;
                String temp = serarchingText.substring(i-wordLength,i);
                if(temp.equals(searchingWord)){
                    if(wholeWordIsSelected()){
                        if(checkForWholeWord(wordLength, serarchingText, add,                                       caretPosition)){
                            caretPosition = i;
                            isFound = true;
                            break;
                        }
                    }
                    else {
                        caretPosition = i;
                        isFound = true;
                        break;
                    }
                }
            }
        }
        ITextEdit.textAreaMain.setCaretPosition(0);
        if(isFound){
            ITextEdit.textAreaMain.requestFocus();
            if(isSearchDown()){
                ITextEdit.textAreaMain.select(caretPosition, caretPosition+wordLength);
            }
            else {
                ITextEdit.textAreaMain.select(caretPosition-wordLength,caretPosition);
            }
            //replace
            if(isReplace){
                String replacementText = replaceText.getText();
                ITextEdit.textAreaMain.replaceSelection(replacementText);
                if(isSearchDown()){
                    ITextEdit.textAreaMain.select(caretPosition,wordLength+replacementText.length());
                }
                else {
                    ITextEdit.textAreaMain.select(caretPosition-replacementText.length(),caretPosition);
                }
            }
            isFoundOne = true;
            return caretPosition;
        }
        
        return -1;
    }
    
    private String getSearchingText(){
        if(!caseNotSelected()){
            return ITextEdit.textAreaMain.getText().toLowerCase();
        }
        return ITextEdit.textAreaMain.getText();
    }
    
    private String getSearchingWord(){
        if(caseNotSelected()){
            return searchText.getText().toLowerCase();
        }
        return searchText.getText();
    }
    
    private boolean caseNotSelected(){
        return !caseSensitive.isSelected();
    }
    
    private boolean isSearchDown(){
        
        return searchDown.isSelected();
    }
    private boolean wholeWordIsSelected(){
        return caseInSensitive.isSelected();
    }
    
    private boolean checkForWholeWord(int check, String text, int add, int caretPosition){
        int offsetLeft = (caretPosition+add)-1;
        int offsetRight = (caretPosition+add)+check;
        if(offsetLeft<0 || offsetRight>text.length())
        {
            return  true;
        }
        
        return ((!Character.isLetterOrDigit(text.charAt(offsetLeft))) && (!Character.isLetterOrDigit(text.charAt(offsetRight))));
    }
    
}
