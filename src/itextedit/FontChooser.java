package itextedit;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Md. Asadul Islam on 3/30/2017.
 *
 * This class demonstrates with choosing font for text area
 */
public class FontChooser extends JDialog implements ListSelectionListener{
    // important variables
    JPanel panel1,panel3;
    JLabel lvlFont,lvlSize,lvlText,lvlPreview;
    JTextField txtFont, txtSize, txtType,txtPreview;
    JScrollPane scrollPaneFont, scrollPaneType, scrollPaneSize;
    JList fontList, sizeList, typeList;
    JButton btnOK, btnCancel;
    GridBagLayout gridBagLayout;
    GridBagConstraints gridBagConstraints;
    String sizeTextInput;
    String[] fonts, types, sizes;
    
    public FontChooser (){
        initComponent();
        
        setTitle("Font");
        setSize(360,400);
        setResizable(false);
        gridBagLayout = new GridBagLayout();
        gridBagConstraints = new GridBagConstraints();
        setLayout(gridBagLayout);
    
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(lvlFont, gridBagConstraints);
        
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(lvlText, gridBagConstraints);
    
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(lvlSize, gridBagConstraints);
    
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(txtFont, gridBagConstraints);
    
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(txtType, gridBagConstraints);
    
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        txtSize.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed (KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_ENTER)
                {
                    try {
                        sizeTextInput = txtSize.getText();
                        if(Integer.parseInt(sizeTextInput)>100){
                            sizeTextInput = "100";
                        }
                        else if(Integer.parseInt(sizeTextInput)<8)
                        {
                            sizeTextInput = "8";
                        }
                        else {
        
                        }
                        Font selectedFont = new Font(String.valueOf(fontList.getSelectedValue()),typeList.getSelectedIndex(),Integer.parseInt(sizeTextInput));
                        lvlPreview.setFont(selectedFont);
                        txtPreview.setFont(selectedFont);
                    }catch ( Exception ew){
                        
                    }
                }
                    
            }
        });
        getContentPane().add(txtSize, gridBagConstraints);
    
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(scrollPaneFont,gridBagConstraints);
    
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(scrollPaneType,gridBagConstraints);
    
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        getContentPane().add(scrollPaneSize,gridBagConstraints);
    
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.CENTER;
        getContentPane().add(lvlPreview, gridBagConstraints);
    
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.anchor = GridBagConstraints.CENTER;
        getContentPane().add(panel3, gridBagConstraints);
    }
    
    @Override
    public void valueChanged (ListSelectionEvent e) {
        try{
            if(e.getSource()==fontList){
                Font selectedFont1 = new Font(String.valueOf(fontList.getSelectedValue()),typeList.getSelectedIndex(),Integer.parseInt((String.valueOf(sizeList.getSelectedValue()))));
                txtFont.setText(String.valueOf(fontList.getSelectedValue()));
                lvlPreview.setFont(selectedFont1);
                txtPreview.setFont(selectedFont1);
            }
            else if(e.getSource()==sizeList){
                txtSize.setText(String.valueOf(sizeList.getSelectedValue()));
                sizeTextInput = txtSize.getText();
                Font selectedFont2 = new Font(String.valueOf(fontList.getSelectedValue()),typeList.getSelectedIndex(),Integer.parseInt(sizeTextInput));
                txtSize.setText(String.valueOf(sizeList.getSelectedValue()));
                lvlPreview.setFont(selectedFont2);
                txtPreview.setFont(selectedFont2);
            }
            else {
                Font selectedFont3 = new Font(String.valueOf(fontList.getSelectedValue()),typeList.getSelectedIndex(),Integer.parseInt((String.valueOf(sizeList.getSelectedValue()))));
                
                txtType.setText(String.valueOf(typeList.getSelectedValue()));
                lvlPreview.setFont(selectedFont3);
                txtPreview.setFont(selectedFont3);
            }
        }catch ( Exception ex ){
            
        }
    }
    
    private void initComponent(){
        lvlFont = new JLabel("Font : ");
        lvlText = new JLabel("Font Style : ");
        lvlSize = new JLabel("Size : ");
        txtFont = new JTextField("", 12);
        txtFont.setEditable(false);
        txtType = new JTextField("Regular", 7);
        txtType.setEditable(false);
        txtSize = new JTextField("12", 3);
    
        txtPreview = new JTextField("AaBbCcDcEe");
        txtPreview.setEditable(false);
        txtPreview.setSize(280,90);
        //txtPreview.setBorder(BorderFactory.createTitledBorder("Preview"));
        //txtPreview.setBorder(BorderFactory.createEtchedBorder());
        txtPreview.setFont(new Font("Arial",Font.PLAIN,12));
    
        fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        fontList = new JList(fonts);
        fontList.setFixedCellWidth(120);
        fontList.addListSelectionListener(this);
        fontList.setSelectedValue("Arial",true);
        scrollPaneFont = new JScrollPane(fontList);
    
        types = new String[] {"Regular", "Bold", "Italic", "Bold & Italic"};
        typeList = new JList(types);
        typeList.setFixedCellWidth(80);
        typeList.addListSelectionListener(this);
        typeList.setSelectedIndex(0);
        scrollPaneType = new JScrollPane(typeList);
    
        sizes = new String[] {"8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "30", "36", "48", "72"};
        sizeList = new JList(sizes);
        sizeList.setFixedCellWidth(30);
        sizeList.addListSelectionListener(this);
        sizeList.setSelectedIndex(4);
        scrollPaneSize = new JScrollPane(sizeList);
    
        panel1 = new JPanel();
        panel1.setSize(300,100);
    
        txtFont.setText(String.valueOf(fontList.getSelectedValue()));
        lvlPreview = new JLabel("AaBbCcDdEe");
        lvlPreview.setBorder(BorderFactory.createTitledBorder("Preview"));
        lvlPreview.setPreferredSize(new Dimension(250,100));
        lvlPreview.setFont(new Font("Arial",Font.PLAIN,12));
        lvlPreview.setHorizontalAlignment(SwingConstants.CENTER);
        lvlPreview.setVerticalAlignment(SwingConstants.CENTER);
       
    
        panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        btnOK = new JButton("OK");
        btnCancel = new JButton("Cancel");
        panel3.add(btnOK);
        panel3.add(btnCancel);
    }
    
    public Font getChooseFont(){
        Font fontSelection = new Font(String.valueOf(fontList.getSelectedValue()), typeList.getSelectedIndex(), Integer.parseInt(String.valueOf(sizeList.getSelectedValue())));
        
        return fontSelection;
    }
    
   
    
    public JButton getBtnOK(){
        return btnOK;
    }
    
    public JButton getBtnCancel(){
        return btnCancel;
    }
}
