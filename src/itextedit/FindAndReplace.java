package itextedit;

import javax.jnlp.JNLPRandomAccessFile;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Md. Asadul Islam on 4/2/2017.
 */
public class FindAndReplace extends JDialog implements ActionListener{
    /**
     * @var txtSearchText  -  is a textfield to get searching word
     * @var txtReplaceText   - is a textfield to get the replacement word to replace searching word
     *
     */
    JTextField txtSearchText, txtReplaceText;
    JCheckBox cbMatchCase, cbMatchWord;
    JRadioButton rbSearchUp, rbSearchDown;
    JLabel lvlSearchStatus;
    final JButton FIND_NEXT = new JButton("Find Next"),
            REPLACE_TEXT = new JButton("Replace Text"),
            REPLACE_ALL = new JButton("Replace All");
    boolean isReplace;
    JPanel panNorthPanel, panCenterPanel, panSouthPanel;
    int caretPosition;
    String searchText, replaceText, allText;
    
    public FindAndReplace(JFrame owner, boolean isReplace){
        super(owner, true);
        this.isReplace = isReplace;
        panNorthPanel = new JPanel();
        panCenterPanel  = new JPanel();
        panSouthPanel = new JPanel();
        
        if(isReplace){
            setTitle("  Find and Replace");
            setReplacePanel(panNorthPanel);
        }
        else {
            setTitle("  Find");
            setFindPanel(panNorthPanel);
        }
        
        addcomponent(panCenterPanel);
        
        lvlSearchStatus = new JLabel();
        panSouthPanel.add(lvlSearchStatus);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing (WindowEvent e) {
                dispose();
            }
        });
        
        getContentPane().add(panNorthPanel, BorderLayout.NORTH);
        getContentPane().add(panCenterPanel, BorderLayout.CENTER);
        getContentPane().add(panSouthPanel, BorderLayout.SOUTH);
        pack();  //allocate actually needed space
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    
    private void addcomponent(JPanel panCenterPanel){
        JPanel panEastPanel = new JPanel();
        JPanel panWestPanel = new JPanel();
        panCenterPanel.setLayout(new GridLayout(1,2));
        panEastPanel.setLayout(new GridLayout(2,1));
        panWestPanel.setLayout(new GridLayout(2,1));
        cbMatchCase = new JCheckBox("Match Case",false);
        cbMatchWord = new JCheckBox("Match Word",false);
        ButtonGroup btnGroup = new ButtonGroup();
        rbSearchUp = new JRadioButton("Search Up", false);
        rbSearchDown = new JRadioButton("Search Down",true);
        btnGroup.add(rbSearchUp);
        btnGroup.add(rbSearchDown);
        panEastPanel.add(cbMatchCase);
        panEastPanel.add(cbMatchWord);
        panEastPanel.setBorder(BorderFactory.createTitledBorder("Search Options :"));
        
        panWestPanel.add(rbSearchUp);
        panWestPanel.add(rbSearchDown);
        panWestPanel.setBorder(BorderFactory.createTitledBorder("Search Direction ; "));
        panCenterPanel.add(panWestPanel);
        panCenterPanel.add(panEastPanel);
    }
    
    private void setFindPanel(JPanel panNorthPanel)
    {
        //JButton FIND_NEXT = new  JButton("Find Next");
        FIND_NEXT.addActionListener(this);
        FIND_NEXT.setEnabled(false);
        txtSearchText = new JTextField(12);
        txtSearchText.addActionListener(this);
        txtSearchText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased (KeyEvent e) {
                boolean state = (txtSearchText.getDocument().getLength()>0);
                FIND_NEXT.setEnabled(state);
            }
            @Override
            public void keyPressed(KeyEvent e){
                if(e.getKeyCode()==KeyEvent.VK_ENTER){
                    findNextWithSelection();
                }
            }
        });
        
        panNorthPanel.setLayout(new FlowLayout());
        panNorthPanel.add(new JLabel("Search Text : "));
        panNorthPanel.add(txtSearchText);
        panNorthPanel.add(FIND_NEXT);
    }
    
    private void setReplacePanel(JPanel panNorthPanel) {
        GridBagLayout grid = new GridBagLayout();
        panNorthPanel.setLayout(grid);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel labelFindWord = new JLabel("Search Text :");
        JLabel labelReplaceWord = new JLabel("Replace With :");
        //final JButton FIND_NEXT = new JButton("Find Next");
        FIND_NEXT.addActionListener(this);
        FIND_NEXT.setEnabled(false);
       // final JButton REPLACE_NEXT = new JButton("Replace Text");
        REPLACE_TEXT.addActionListener(this);
        REPLACE_TEXT.setEnabled(false);
       // final JButton REPLACE_ALL = new JButton("Replace All");
        REPLACE_ALL.addActionListener(this);
        REPLACE_ALL.setEnabled(false);
        txtSearchText = new JTextField(12);
        txtReplaceText = new JTextField(12);
        txtSearchText.addKeyListener(replaceKeyListener);
        txtReplaceText.addKeyListener(replaceKeyListener);
        
        constraints.gridx = 0;
        constraints.gridy = 0;
        grid.setConstraints(labelFindWord,constraints);
        panNorthPanel.add(labelFindWord);
        
        constraints.gridx = 1;
        constraints.gridy = 0;
        grid.setConstraints(txtSearchText,constraints);
        panNorthPanel.add(txtSearchText);
    
        constraints.gridx = 2;
        constraints.gridy = 0;
        grid.setConstraints(FIND_NEXT,constraints);
        panNorthPanel.add(FIND_NEXT);
        
        constraints.gridx = 0;
        constraints.gridy = 1;
        grid.setConstraints(labelReplaceWord,constraints);
        panNorthPanel.add(labelReplaceWord);
        
        constraints.gridx = 1;
        constraints.gridy = 1;
        grid.setConstraints(txtReplaceText,constraints);
        panNorthPanel.add(txtReplaceText);
        
        constraints.gridx = 2;
        constraints.gridy = 1;
        grid.setConstraints(REPLACE_TEXT,constraints);
        panNorthPanel.add(REPLACE_TEXT);
        
        constraints.gridx = 2;
        constraints.gridy = 2;
        grid.setConstraints(REPLACE_ALL,constraints);
        panNorthPanel.add(REPLACE_ALL);
    }
    
    void findNext() {
        
        allText = ITextEdit.textAreaMain.getText();
        searchText = txtSearchText.getText();
        
        caretPosition = ITextEdit.textAreaMain.getCaretPosition();
        
        int caretStart = ITextEdit.textAreaMain.getSelectionStart();
        int caretEnd = ITextEdit.textAreaMain.getSelectionEnd();
        
        if (rbSearchUp.isSelected()) {
            if (caretStart != caretEnd)
                caretPosition = caretEnd - searchText.length() - 1;
            /*****
             * Notepad doesnt use the else part, but it should be, instead of
             * using caretPosition.*** else caretPosition=caretPosition-searchText.length();
             ******/
            
            if (!cbMatchCase.isSelected())
                caretPosition = allText.toUpperCase().lastIndexOf(searchText.toUpperCase(), caretPosition);
            else
                caretPosition = allText.lastIndexOf(searchText, caretPosition);
        } else {
            if (caretStart != caretEnd)
                caretPosition = caretStart + 1;
            if (!cbMatchCase.isSelected())
                caretPosition = allText.toUpperCase().indexOf(searchText.toUpperCase(), caretPosition);
            else
                caretPosition = allText.indexOf(searchText, caretPosition);
        }
        
    }
    
    ///////////////////////////////////////////////
    public void findNextWithSelection() {
        findNext();
        if (caretPosition != -1) {
            ITextEdit.textAreaMain.setSelectionStart(caretPosition);
            ITextEdit.textAreaMain.setSelectionEnd(caretPosition + txtSearchText.getText().length());
        } else
            JOptionPane.showMessageDialog(this, "End of File");
    }
    
    void replaceNext() {
        // if nothing is selectd
        if (ITextEdit.textAreaMain.getSelectionStart() == ITextEdit.textAreaMain.getSelectionEnd()) {
            findNextWithSelection();
            return;
        }
        
        String searchText = txtSearchText.getText();
        String temp = ITextEdit.textAreaMain.getSelectedText(); // get selected text
        
        // check if the selected text matches the search text then do
        // replacement
        
        if ((cbMatchCase.isSelected() && temp.equals(searchText))
                || (!cbMatchCase.isSelected() && temp.equalsIgnoreCase(searchText))) {
            ITextEdit.textAreaMain.replaceSelection(txtReplaceText.getText());
            ITextEdit.textAreaMain.setSelectionStart(caretPosition);
            ITextEdit.textAreaMain.setSelectionEnd(caretPosition + txtSearchText.getText().length());
        }
        //findNextWithSelection();
    }
    
    void replaceAll() {
        /*if (rbSearchUp.isSelected())
            caretPosition = ITextEdit.textAreaMain.getText().length() - 1;
            //ITextEdit.textAreaMain.setCaretPosition(ITextEdit.textAreaMain.getText().length() - 1);
        else
            caretPosition = 0;*/
            //ITextEdit.textAreaMain.setCaretPosition(0);
        
        int idx = 0;
        int counter = 0;
        caretPosition = 0;
        do {
            findNext();
            if (caretPosition == -1)
                break;
            counter++;
            ITextEdit.textAreaMain.replaceRange(txtReplaceText.getText(),caretPosition, caretPosition + txtSearchText.getText().length());
        } while (caretPosition != -1);
        
        //return counter;
    }
    
    KeyListener replaceKeyListener = new KeyAdapter(){
        @Override
        public void keyReleased (KeyEvent e) {
            boolean state = (txtReplaceText.getDocument().getLength()>0) && (txtSearchText.getDocument().getLength()>0);
            FIND_NEXT.setEnabled(state);
            REPLACE_TEXT.setEnabled(state);
            REPLACE_ALL.setEnabled(state);
        }
    };
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==FIND_NEXT){
            findNextWithSelection();
        }
        else if(e.getSource()==REPLACE_TEXT){
            replaceNext();
        }
        else if(e.getSource()==REPLACE_ALL){
            replaceAll();
        }
    }
    
    
    private boolean checkForWholeWord(int check, String text, int add, int caretPosition){
        int offsetLeft = (caretPosition+add)-1;
        int offsetRight = (caretPosition+add)+check;
        if(offsetLeft<0 || offsetRight>text.length())
        {
            return  true;
        }
        
        return ((!Character.isLetterOrDigit(text.charAt(offsetLeft))) && (!Character.isLetterOrDigit(text.charAt(offsetRight))));
    }
    
}
